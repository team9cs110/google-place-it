package com.cse110.group9.android.placeitlist;

import java.util.UUID;

import android.support.v4.app.Fragment;

public class PlaceItActivity extends SingleFragmentActivity 
{
	@Override
    protected Fragment createFragment() 
	  {
		//getting the post its stored in CrimeFragment from the intent that started this Activity
        UUID placeItId = (UUID)getIntent().getSerializableExtra(PlaceItFragment.EXTRA_PLACE_IT_ID);
        return PlaceItFragment.newInstance(placeItId);
    }
}
