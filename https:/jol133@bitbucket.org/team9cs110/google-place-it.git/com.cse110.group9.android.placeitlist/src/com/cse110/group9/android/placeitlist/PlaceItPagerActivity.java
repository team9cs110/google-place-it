package com.cse110.group9.android.placeitlist;

import java.util.ArrayList;
import java.util.UUID;

import com.cse110.group9.android.placeitlist.R;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.view.ViewPager;

// Swiping to pages through PlaceIts. It will create and manage ViewPager
public class PlaceItPagerActivity extends FragmentActivity 
{
    ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //creates  a view pager
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        //gets data set from class and then get the activity instance from fragment manager
        final ArrayList<PlaceIt> mPlaceIts = PlaceItListData.get(this).getPlaceIts();

        FragmentManager fm = getSupportFragmentManager();
        //manages data model
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            
        		//gets total number of elements stored in the list
        		@Override
            public int getCount() 
        		{
                return mPlaceIts.size();
            }
        		//returns
            @Override
            public Fragment getItem(int pos) 
            {
                UUID PlaceItId =  mPlaceIts.get(pos).getId();
                return PlaceItFragment.newInstance(PlaceItId);
            }
        }); 

        UUID PlaceItId = (UUID)getIntent().getSerializableExtra(PlaceItFragment.EXTRA_PLACE_IT_ID);
        for (int i = 0; i < mPlaceIts.size(); i++) {
            if (mPlaceIts.get(i).getId().equals(PlaceItId)) 
            {
                mViewPager.setCurrentItem(i);
                break;
            } 
        }
    }
}
