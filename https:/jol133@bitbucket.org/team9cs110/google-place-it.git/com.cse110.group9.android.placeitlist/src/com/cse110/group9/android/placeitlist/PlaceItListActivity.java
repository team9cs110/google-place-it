package com.cse110.group9.android.placeitlist;

import android.support.v4.app.Fragment;

public class PlaceItListActivity extends SingleFragmentActivity 
{

    @Override
    protected Fragment createFragment() 
    {
        return new PlaceItListFragment();
    }
}
