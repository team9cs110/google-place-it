package com.cse110.group9.android.placeitlist;

import java.util.ArrayList;
import java.util.UUID;

import android.content.Context;

import android.util.Log;

public class PlaceItListData 
{
    private static final String TAG = "PlaceItListData";
    private static final String FILENAME = "PlaceIts.json";

    private ArrayList<PlaceIt> mPlaceIts;
    private PlaceItIntentJSONSerializer mSerializer;

    private static PlaceItListData sPlaceItListData;
    private Context mAppContext;

    private PlaceItListData(Context appContext) 
    {
        mAppContext = appContext;
        mSerializer = new PlaceItIntentJSONSerializer(mAppContext, FILENAME);

        try 
        {
            mPlaceIts = mSerializer.loadPlaceIts();
        } 
        catch (Exception e) 
        {
            mPlaceIts = new ArrayList<PlaceIt>();
            Log.e(TAG, "Error loading PlaceIts: ", e);
        }
    }

    public static PlaceItListData get(Context c) 
    {
        if (sPlaceItListData == null) 
        {
            sPlaceItListData = new PlaceItListData(c.getApplicationContext());
        }
        return sPlaceItListData;
    }

    public PlaceIt getPlaceIt(UUID id) 
    {
        for (PlaceIt c : mPlaceIts) 
        {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }
    
    public void addPlaceIt(PlaceIt c)
    {
        mPlaceIts.add(c);
        savePlaceIts();
    }

    public ArrayList<PlaceIt> getPlaceIts() 
    {
        return mPlaceIts;
    }

    public void deletePlaceIt(PlaceIt c) 
    {
        mPlaceIts.remove(c);
        savePlaceIts();
    }

    public boolean savePlaceIts() 
    {
        try 
        {
            mSerializer.savePlaceIts(mPlaceIts);
            Log.d(TAG, "PlaceIts saved to file");
            return true;
        }
        catch (Exception e) 
        {
            Log.e(TAG, "Error saving PlaceIts: " + e);
            return false;
        }
    }
}

